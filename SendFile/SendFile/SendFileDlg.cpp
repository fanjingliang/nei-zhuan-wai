// SendFileDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "SendFile.h"
#include "SendFileDlg.h"
#include "ScreenDraw.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSendFileDlg 对话框




CSendFileDlg::CSendFileDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSendFileDlg::IDD, pParent)
	, m_static_info(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSendFileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_STATIC_INFO, m_static_info);
}

BEGIN_MESSAGE_MAP(CSendFileDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_SEND, &CSendFileDlg::OnBnClickedButtonSend)
	ON_BN_CLICKED(IDC_BUTTON_RECV, &CSendFileDlg::OnBnClickedButtonRecv)
	ON_BN_CLICKED(IDC_BUTTON_SELECTFILE, &CSendFileDlg::OnBnClickedButtonSelectfile)
	ON_BN_CLICKED(IDOK, &CSendFileDlg::OnBnClickedOk)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CSendFileDlg::OnBnClickedButtonStop)
END_MESSAGE_MAP()


// CSendFileDlg 消息处理程序

BOOL CSendFileDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	//使窗口居中，窗口大小定为 600*500

	int nwidth = ::GetSystemMetrics(SM_CXSCREEN);
	int nheight = ::GetSystemMetrics(SM_CYSCREEN);

	InitControlPosition();

	m_recvlist = NULL;
	m_recvlist_count = 0;
	m_recvlist_cur = 0;
	memset(m_recv_file,0, 256);

	m_pagelist = NULL;
	m_pagelist_count = 0;
	m_pagelist_index = 0;

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CSendFileDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CSendFileDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CSendFileDlg::OnBnClickedButtonSend()
{
	// TODO: 在此添加控件通知处理程序代码
	if(m_send_filepath != "")
	{
		m_pagelist = MakeFilePageList(m_send_filepath.GetBuffer(), m_send_file.GetBuffer(),&m_pagelist_count);
	}

	CWnd * pwnd;
	pwnd = GetDlgItem(IDC_BUTTON_RECV);
	pwnd->EnableWindow(false);
	pwnd = GetDlgItem(IDC_BUTTON_SEND);
	pwnd->EnableWindow(false);
	SetTimer(2,300,NULL);

}

void CSendFileDlg::OnBnClickedButtonRecv()
{
	// TODO: 在此添加控件通知处理程序代码
	//KillTimer(2);	
	
	g_picture_rect.left = g_picture_rect.right = 0;
	g_picture_rect.top = g_picture_rect.bottom = 0;

	CWnd * pwnd;
	pwnd = GetDlgItem(IDC_BUTTON_SEND);
	pwnd->EnableWindow(false);
	pwnd = GetDlgItem(IDC_BUTTON_RECV);
	pwnd->EnableWindow(false);

	SetTimer(1,50,NULL);
	
}

void CSendFileDlg::OnBnClickedButtonSelectfile()
{
	// TODO: 在此添加控件通知处理程序代码
	CFileDialog dlgfile(TRUE,NULL,NULL,OFN_HIDEREADONLY, _T("All file(*.*)|*.*||"),NULL);
	if (dlgfile.DoModal())
	{
		m_send_filepath = dlgfile.GetPathName();
		m_send_file = dlgfile.GetFileName();
	}
}

void CSendFileDlg::OnBnClickedOk()
{
	OnBnClickedButtonStop();
	OnOK();
}

void CSendFileDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	switch(nIDEvent)
	{
	case 1:
		{
			if(g_picture_rect.left == 0 && g_picture_rect.right == 0)
			{
				GetPictureRect();

				UpdateData(true);
				m_static_info.Format("left:%d top:%d right:%d bottom:%d", 
					g_picture_rect.left,
					g_picture_rect.top,
					g_picture_rect.right,
					g_picture_rect.bottom);
				UpdateData(false);

			}
			else
			{
				char * buf;
				if(GetPage(&buf) == true)
				{
					ParseFile((screenfileinfo_t*)buf);
				}
			}
			break;
		}
	case 2:
		{
			CWnd * pwnd = GetDlgItem(IDC_STATIC_INFO);
			CDC * pdc = pwnd->GetDC();
			pwnd->Invalidate();
			pwnd->UpdateWindow();
			
			DrawPos(pdc);

			if(m_pagelist_index < m_pagelist_count)
			{
				DrawPageDirect(pdc,m_pagelist, m_pagelist_index);
				m_pagelist_index ++;
			}

			if(m_pagelist_index >= m_pagelist_count)
			{
				m_pagelist_index = 0;
			}
			GetDlgItem(IDC_STATIC_INFO)->ReleaseDC(pdc);

		}
	}

	CDialog::OnTimer(nIDEvent);
}

void CSendFileDlg::ClearLastFile()
{
	if(m_recvlist != NULL)
	{
		for(int i = 0 ; i < m_recvlist_count ; i++)
		{
			free(m_recvlist[i]);
		}
		free(m_recvlist);
	}
	m_recvlist = NULL;
	m_recvlist_count = 0;
	m_recvlist_cur = 0;
	memset(m_recv_file,0, 256);
}

void CSendFileDlg::ParseFile(screenfileinfo_t * info)
{
	static SYSTEMTIME lasttm;

	if(	lasttm.wDayOfWeek == info->info.tm.wDayOfWeek &&
		lasttm.wHour == info->info.tm.wHour &&
		lasttm.wMilliseconds == info->info.tm.wMilliseconds &&
		lasttm.wMinute == info->info.tm.wMinute &&
		lasttm.wMonth == info->info.tm.wMonth &&
		lasttm.wSecond == info->info.tm.wSecond &&
		lasttm.wYear == info->info.tm.wYear)
		return;

	int w = g_picture_rect.right-g_picture_rect.left;
	int h = g_picture_rect.bottom-g_picture_rect.top;
	int pagesize = w*h*2;

	if(strlen(m_recv_file) == 0)
	{
		strcpy(m_recv_file,info->info.file);
	}
	else if(strlen(m_recv_file) > 0 && strcmp(info->info.file,m_recv_file) != 0)
	{
		ClearLastFile();
	}

	if(m_recvlist == NULL)
	{
		m_recvlist = (char **)malloc(sizeof(char *)*info->info.pagecount);
		m_recvlist_count = info->info.pagecount;
		memset(m_recvlist,0,sizeof(char*)*info->info.pagecount);
	}

	if(m_recvlist[info->info.pagecur] != NULL)
	{
		//显示当前接收状态
		UpdateRecvStat(info->info.file, info->info.pagecur);
		return;
	}
	else
	{
		m_recvlist[info->info.pagecur] = (char *)malloc(pagesize);
		memcpy(m_recvlist[info->info.pagecur], info->info.data, pagesize);
		m_recvlist_cur ++;

		if(m_recvlist_cur == m_recvlist_count)
		{
			//savefile
			FILE *fp;
			fp =fopen(m_recv_file,"wb");
			if(fp != NULL)
			{
				for(int i = 0 ; i < m_recvlist_count -1; i++)
				{
					fwrite(m_recvlist[i],1,pagesize,fp);
				}
				fwrite(m_recvlist[m_recvlist_count-1],1,info->info.len-pagesize*(m_recvlist_count-1),fp);
				fclose(fp);
			}
			UpdateRecvStat(info->info.file, info->info.pagecur);
			ClearLastFile();
			lasttm = info->info.tm;
		}
		else
		{
			//显示当前接收状态
			UpdateRecvStat(info->info.file, info->info.pagecur);
		}
	}

}

void CSendFileDlg::UpdateRecvStat(char * file, int cur)
{
	UpdateData(TRUE);
	if( m_recvlist_count == m_recvlist_cur)
	{
		m_static_info.Format("文件 %s 已收到.", file);
	}
	else
	{
		m_static_info.Format("文件 %s 总页数%d 当前传输页%d, 已收到", file,m_recvlist_count,cur);

		int start = -1;
		int end = -1;
		for(int i = 0 ; i < m_recvlist_count; i++)
		{
			if(m_recvlist[i] != NULL)
			{
				if( start == -1)
				{
					start = i;
					end = i;
				}
				else
				{
					if( (end+1) == i)
					{
						end = i;
					}
					else
					{
						m_static_info.AppendFormat(" %d-%d,", start,end);
						start = i;
						end = i;
					}
				}
			}
		}
		if(start != -1)
			m_static_info.AppendFormat(" %d-%d,", start,end);
	}
	UpdateData(FALSE);
}

void CSendFileDlg::InitControlPosition()
{
	int nwidth = ::GetSystemMetrics(SM_CXSCREEN);
	int nheight = ::GetSystemMetrics(SM_CYSCREEN);

	RECT rc;
	rc.left = (nwidth-WINDOWWIDTH)/2;
	rc.top = (nheight-WINDOWHEIGHT)/2;
	rc.bottom = rc.top+WINDOWHEIGHT;
	rc.right = rc.left+WINDOWWIDTH;

	MoveWindow(&rc);

	GetClientRect(&rc);

	CWnd * pwnd;
	CRect crc;
	pwnd = GetDlgItem(IDC_STATIC_INFO);
	pwnd->MoveWindow(20,70, rc.right-rc.left -2*20, rc.bottom-rc.top - 70-20);
	pwnd->GetClientRect(&rc);
	g_picture_rect = rc;

	pwnd = GetDlgItem(IDC_BUTTON_SELECTFILE);
	pwnd->GetClientRect(&rc);
	crc.CopyRect(&rc);
	crc.MoveToXY(20,20);
	pwnd->MoveWindow(LPRECT(crc));

	pwnd = GetDlgItem(IDC_BUTTON_SEND);
	pwnd->GetClientRect(&rc);
	crc.CopyRect(&rc);
	crc.MoveToXY(140,20);
	pwnd->MoveWindow(LPRECT(crc));
	
	pwnd = GetDlgItem(IDC_BUTTON_RECV);
	pwnd->GetClientRect(&rc);
	crc.CopyRect(&rc);
	crc.MoveToXY(260,20);
	pwnd->MoveWindow(LPRECT(crc));

	pwnd = GetDlgItem(IDC_BUTTON_STOP);
	pwnd->GetClientRect(&rc);
	crc.CopyRect(&rc);
	crc.MoveToXY(380,20);
	pwnd->MoveWindow(LPRECT(crc));

	pwnd = GetDlgItem(IDOK);
	pwnd->GetClientRect(&rc);
	crc.CopyRect(&rc);
	crc.MoveToXY(500,20);
	pwnd->MoveWindow(LPRECT(crc));
	
}
void CSendFileDlg::OnBnClickedButtonStop()
{
	// TODO: 在此添加控件通知处理程序代码
	//停止发送和接收，清除缓存的数据，并将状态恢复原始。

	KillTimer(1);
	KillTimer(2);
	::Sleep(1000); 
	//加入等待是确定ontime处理完毕

	FreeFilePageList(m_pagelist, m_pagelist_count);
	m_pagelist = NULL;
	m_pagelist_index = 0;
	m_pagelist_count = 0;

	ClearLastFile();

	CWnd * pwnd;
	pwnd = GetDlgItem(IDC_BUTTON_SEND);
	pwnd->EnableWindow(true);

	pwnd = GetDlgItem(IDC_BUTTON_RECV);
	pwnd->EnableWindow(true);
}
