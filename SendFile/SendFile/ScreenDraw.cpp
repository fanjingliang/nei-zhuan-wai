
#include "stdafx.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

#include "ScreenDraw.h"

RECT g_picture_rect;

COLORREF GetRGBFromScreenBuf(char * screenbuf, int w, int h, int x, int y)
{
	unsigned int n = x*4+y*w*4;

	return RGB(screenbuf[n+2], screenbuf[n+1], screenbuf[n]);
}

void SetRGBToScreenBuf(char * screenbuf, int w, int h, int x, int y, COLORREF c)
{
	unsigned int n = x*4+y*w*4;
	unsigned char r = GetRValue(c);
	unsigned char g = GetGValue(c);
	unsigned char b = GetBValue(c);

	screenbuf[n+2] = r;
	screenbuf[n+1] = g;
	screenbuf[n+0] = b;
}
COLORREF GetRGBFromBuf(char * buf, int w, int h, int x, int y)
{
	unsigned int n = x*3+y*w*3;

	return RGB(buf[n+2], buf[n+1], buf[n]);
}

void SetRGBToBuf(char * buf, int w, int h, int x, int y, COLORREF c)
{
	unsigned int n = x*3+y*w*3;
	unsigned char r = GetRValue(c);
	unsigned char g = GetGValue(c);
	unsigned char b = GetBValue(c);

	buf[n+2] = r;
	buf[n+1] = g;
	buf[n+0] = b;
}

/*
	将普通内存，转为屏幕内存

	普通内存的长度为 w*h*3
	屏幕内存的长度为 w*h*4
*/
void Buf2ScreenBuf(int w,int h, char * buf, char * screenbuf)
{
	for(int i=0 ; i < w; i++)
	{
		for(int j = 0; j < h; j++)
		{
			COLORREF c = GetRGBFromBuf(buf,w,h,i,j);
			SetRGBToScreenBuf(screenbuf,w,h,i,j,c);
		}
	}
}

void ScreenBuf2Buf(int w,int h, char *buf, char * screenbuf)
{
	for(int i=0 ; i < w; i++)
	{
		for(int j = 0; j < h; j++)
		{
			COLORREF c = GetRGBFromScreenBuf(screenbuf,w,h,i,j);
			SetRGBToBuf(buf,w,h,i,j,c);
		}
	}
}

/*
	在 x,y 这位置，画图，宽度w,高度h
	可填入的数据的最大长度为 w*h*3
	当前填入的数据的指针buf
*/
void ToScreen(CDC *pdc, int x,int y, int w, int h, char * buf)
{
	CDC memdc;
	CBitmap bmp;

	memdc.CreateCompatibleDC(pdc);
	bmp.CreateCompatibleBitmap(pdc , w,h);

	CGdiObject *pold = memdc.SelectObject(&bmp);

	

	BITMAPINFO bmpInfo;
	bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmpInfo.bmiHeader.biWidth = w;
	bmpInfo.bmiHeader.biHeight = h;
	bmpInfo.bmiHeader.biPlanes = 1;
	bmpInfo.bmiHeader.biBitCount = 32;
	bmpInfo.bmiHeader.biClrImportant = 0;
	bmpInfo.bmiHeader.biClrUsed = 0;
	bmpInfo.bmiHeader.biCompression = 0;
	bmpInfo.bmiHeader.biSizeImage = w*h*4;
	bmpInfo.bmiHeader.biXPelsPerMeter = 0;
	bmpInfo.bmiHeader.biYPelsPerMeter = 0;

	::SetDIBits(HDC(memdc) , HBITMAP(bmp) ,0,h,buf, &bmpInfo,DIB_RGB_COLORS);
	
	pdc->BitBlt(x,y,w,h,&memdc,0,0,SRCCOPY);
	
	memdc.SelectObject(pold);
	bmp.DeleteObject();
	memdc.DeleteDC();
	
}

void FromScreen(int x, int y, int w, int h, char * buf)
{
	HDC hdc;
	HDC memdc;
	HBITMAP hbmp;

	hdc = ::GetDC(NULL);
	memdc = ::CreateCompatibleDC(hdc);
	hbmp = ::CreateCompatibleBitmap(hdc, w,h);

	BITMAP bm;
	::GetObject(hbmp,sizeof(BITMAP),&bm);

	HGDIOBJ hold = ::SelectObject(memdc, hbmp);
	::BitBlt(memdc,0,0,w,h,hdc,x,y,SRCCOPY);

	BITMAPINFO bmpInfo;
	bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmpInfo.bmiHeader.biWidth = bm.bmWidth;
	bmpInfo.bmiHeader.biHeight = bm.bmHeight;
	bmpInfo.bmiHeader.biPlanes = 1;
	bmpInfo.bmiHeader.biBitCount = bm.bmBitsPixel;
	bmpInfo.bmiHeader.biClrImportant = 0;
	bmpInfo.bmiHeader.biClrUsed = 0;
	bmpInfo.bmiHeader.biCompression = 0;
	bmpInfo.bmiHeader.biSizeImage = bm.bmWidthBytes*bm.bmHeight;
	bmpInfo.bmiHeader.biXPelsPerMeter = 0;
	bmpInfo.bmiHeader.biYPelsPerMeter = 0;

	::GetDIBits(memdc , hbmp ,0,bm.bmHeight,buf, &bmpInfo,DIB_RGB_COLORS);

	SaveBitmapToFile(hbmp,"out.bmp");

	::SelectObject(memdc, hold);
	::DeleteObject(hbmp);
	::DeleteDC(memdc);
	::ReleaseDC(NULL,hdc);
}

unsigned int GetCheckSum(char * buf, int len)
{
	unsigned int inum = len/4;
	unsigned int *pbuf = (unsigned int *) buf;

	unsigned int sum = 0;
	for(int i = 0 ; i < inum; i++)
	{
		sum += pbuf[i];
	}
	return sum;
}

void DrawPos(CDC *pdc)
{
	static char scrbuf[WINDOWWIDTH*WINDOWHEIGHT*4];
	memset(scrbuf,255,sizeof(scrbuf));
	int i;
	int w = g_picture_rect.right-g_picture_rect.left;
	int h = g_picture_rect.bottom-g_picture_rect.top;

	for(i = g_picture_rect.left; i < g_picture_rect.right ; i++)
	{
		SetRGBToScreenBuf(scrbuf,w,h,i,g_picture_rect.top, RGB(11,22,33));
	}
	for(i = g_picture_rect.top; i < g_picture_rect.bottom ; i++)
	{
		SetRGBToScreenBuf(scrbuf,w,h,g_picture_rect.left,i, RGB(11,22,33));
	}

	for(i = g_picture_rect.left; i < g_picture_rect.right ; i++)
	{
		SetRGBToScreenBuf(scrbuf,w,h,i,g_picture_rect.top+h-1, RGB(11,22,33));
	}
	
	for(i = g_picture_rect.top; i < g_picture_rect.bottom ; i++)
	{
		SetRGBToScreenBuf(scrbuf,w,h,g_picture_rect.left+w-1,i, RGB(11,22,33));
	}

	ToScreen(pdc, g_picture_rect.left,g_picture_rect.top ,w,h, scrbuf);
}

void DrawPage( CDC *pdc, fileinfo_t * pinfo)
{
	static char buf[WINDOWWIDTH*WINDOWHEIGHT*3];
	static char scrbuf[WINDOWWIDTH*WINDOWHEIGHT*4];

	int w = g_picture_rect.right-g_picture_rect.left-2;
	int h = g_picture_rect.bottom-g_picture_rect.top-2;

	screenfileinfo_t * pscreen = (screenfileinfo_t*)buf;
	pscreen->checksum = GetCheckSum((char *)&pscreen->info,sizeof(pscreen->info));
	memcpy(&pscreen->info, pinfo, sizeof(fileinfo_t));
	
	Buf2ScreenBuf(w,h,buf,scrbuf);

	ToScreen(pdc,g_picture_rect.left+1,g_picture_rect.top+1,w,h, scrbuf);
}

void DrawPageDirect(CDC *pdc, char ** ppagelist, int index)
{
	int w = g_picture_rect.right-g_picture_rect.left-2;
	int h = g_picture_rect.bottom-g_picture_rect.top-2;

	ToScreen(pdc,g_picture_rect.left+1,g_picture_rect.top+1,w,h, ppagelist[index]);
}

bool GetPage( char ** out_buf)
{
	static char scrbuf[WINDOWWIDTH*WINDOWHEIGHT*4];
	static char buf[WINDOWWIDTH*WINDOWHEIGHT*3];

	int w = g_picture_rect.right-g_picture_rect.left;
	int h = g_picture_rect.bottom-g_picture_rect.top;

	int pagesize = w*h*2;

	FromScreen(g_picture_rect.left,g_picture_rect.top,w,h, scrbuf);
	ScreenBuf2Buf(w,h,buf,scrbuf);

	screenfileinfo_t * pscreen = (screenfileinfo_t*)buf;

	unsigned int csum = GetCheckSum((char *)&pscreen->info,sizeof(pscreen->info)+pagesize);
	if(csum == pscreen->checksum)
	{
		*out_buf = buf;
		return true;
	}
	else
	{
		return false;
	}
}

char** MakeFilePageList(char *pathfile, char *file, int * out_num)
{
	int w = g_picture_rect.right-g_picture_rect.left-2;
	int h = g_picture_rect.bottom-g_picture_rect.top-2;

	unsigned int pagesize = w*h*2;

	struct _stat lstat;
	_stat(pathfile, &lstat);
	unsigned int len = lstat.st_size;
	
	unsigned int pagenum;
	
	if( len % pagesize == 0)
		pagenum = len /pagesize;
	else
		pagenum = len/pagesize + 1;

	int i;
	
	FILE *fp = fopen(pathfile,"rb");
	if(fp == NULL) return NULL;
	
	//多申请一个，最后一个置空表示结束
	char ** ppagelist = (char **)malloc(sizeof(char*)*(pagenum));
	memset(ppagelist,0,sizeof(char *)*(pagenum));
	char *ppage;

	static char buf[WINDOWWIDTH*WINDOWHEIGHT*3];
	screenfileinfo_t * pscreen = (screenfileinfo_t*)buf;

	SYSTEMTIME tm;
	::GetSystemTime(&tm);

	for(i= 0; i < pagenum; i++)
	{
		strcpy(pscreen->info.file,file);
		pscreen->info.len = len;
		pscreen->info.pagecount = pagenum;
		pscreen->info.pagecur = i;
		pscreen->info.tm = tm;
		fread(pscreen->info.data ,1,pagesize,fp);
		
		pscreen->checksum = GetCheckSum((char *)&pscreen->info,sizeof(pscreen->info)+pagesize);
		ppage = (char *)malloc(w*h*4);
		
		Buf2ScreenBuf(w,h,buf,ppage);

		ppagelist[i] = ppage;
	}
	
	fclose(fp);

	*out_num = pagenum;
	return ppagelist;
}

void FreeFilePageList( char **plist,int num)
{
	if(plist == NULL) return;
	int i = 0;
	for(i = 0; i < num; i++)
	{
		free(plist[i]);
	}
	free(plist);	
}

/*
	通过图框来定位图片的矩形位置
*/
void GetPictureRect()
{
	static char scrbuf[SCREENWIDTH*SCREENHEIGHT*4];

	FromScreen(0, 0,SCREENWIDTH,SCREENHEIGHT, scrbuf);

	int i, j;
	for(i = 0 ; i < SCREENWIDTH; i++)
	{
		for(j = 0 ; j < SCREENHEIGHT; j++)
		{
			COLORREF color = GetRGBFromScreenBuf(scrbuf,SCREENWIDTH,SCREENHEIGHT,i,j);

			//找到左上角的一个点了，分别向右和向下判断宽度和高度
			if(color == RGB(11,22,33))
			{
				int k;
				//判断宽度
				int nwidth = 0;
				for(k = i ; k < SCREENWIDTH; k ++)
				{
					color = GetRGBFromScreenBuf(scrbuf,SCREENWIDTH,SCREENHEIGHT,k,j);
					if(color == RGB(11,22,33))
					{
						nwidth ++;
					}
					else
					{
						break;
					}
				}
				if(nwidth < 10)
				{
					continue;
				}

				//判断高度
				int nheight = 0;
				for(k = j; k < SCREENHEIGHT; k++)
				{
					color = GetRGBFromScreenBuf(scrbuf,SCREENWIDTH,SCREENHEIGHT,i,k);
					if(color == RGB(11,22,33))
					{
						nheight ++;
					}
					else
					{
						break;
					}
				}
				if(nheight < 10)
				{
					continue;
				}

				//判断下宽度
				int nwidth2 = 0;
				for(k = i ; k < SCREENWIDTH; k ++)
				{
					color = GetRGBFromScreenBuf(scrbuf,SCREENWIDTH,SCREENHEIGHT,k,j+nheight-1);
					if(color == RGB(11,22,33))
					{
						nwidth2 ++;
					}
					else
					{
						break;
					}
				}
				if(nwidth != nwidth2)
				{
					continue;
				}

				//判断右高度
				int nheight2 = 0;
				for(k = j; k < SCREENHEIGHT; k++)
				{
					color = GetRGBFromScreenBuf(scrbuf,SCREENWIDTH,SCREENHEIGHT,i+nwidth-1,k);
					if(color == RGB(11,22,33))
					{
						nheight2 ++;
					}
					else
					{
						break;
					}
				}
				if(nheight != nheight2)
				{
					continue;
				}


				g_picture_rect.left = i + 1;
				g_picture_rect.bottom = SCREENHEIGHT-(j + 1);
				g_picture_rect.right = i+nwidth-1;
				g_picture_rect.top = SCREENHEIGHT-(j+nheight-1-1)-1;
				return;

			}
		}
	}
	
}

void SaveBitmapToFile(HBITMAP hBitmap, LPSTR lpFileName)
{
	//lpFileName 为位图文件名  
	HDC hDC;  
	 //设备描述表  
	int iBits;  
	//当前显示分辨率下每个像素所占字节数  
	WORD wBitCount;  
	 //位图中每个像素所占字节数  
	//定义调色板大小， 位图中像素字节大小 ， 位图文件大小 ， 写入文件字节数  
	DWORD dwPaletteSize=0,dwBmBitsSize,dwDIBSize, dwWritten;  
	BITMAP Bitmap;  
	//位图属性结构  
	BITMAPFILEHEADER bmfHdr;  
	//位图文件头结构  
	BITMAPINFOHEADER bi;  
	//位图信息头结构  
	LPBITMAPINFOHEADER lpbi;  
	//指向位图信息头结构  
	HANDLE fh, hDib, hPal;  
	HPALETTE hOldPal=NULL;  
	//定义文件，分配内存句柄，调色板句柄  
	  
	//计算位图文件每个像素所占字节数  
	hDC = CreateDC("DISPLAY",NULL,NULL,NULL);  
	iBits = GetDeviceCaps(hDC, BITSPIXEL) *  
	GetDeviceCaps(hDC, PLANES);  
	DeleteDC(hDC);  
	if (iBits <= 1)  
	wBitCount = 1;  
	else if (iBits <= 4)  
	wBitCount = 4;  
	else if (iBits <= 8)  
	wBitCount = 8;  
	else if (iBits <= 24)  
	wBitCount = 24;  
	else  
	wBitCount = 32;  
	//计算调色板大小  
	if (wBitCount <= 8)  
	dwPaletteSize=(1<<wBitCount)*sizeof(RGBQUAD);  
	 
	//设置位图信息头结构  
	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&Bitmap);  
	bi.biSize = sizeof(BITMAPINFOHEADER);  
	bi.biWidth = Bitmap.bmWidth;  
	bi.biHeight = Bitmap.bmHeight;  
	bi.biPlanes = 1;  
	bi.biBitCount = wBitCount;  
	bi.biCompression = BI_RGB;  
	bi.biSizeImage = 0;  
	bi.biXPelsPerMeter = 0;  
	bi.biYPelsPerMeter = 0;  
	bi.biClrUsed = 0;  
	bi.biClrImportant = 0;  
	  
	dwBmBitsSize = ((Bitmap.bmWidth*wBitCount+31)/32)*4*Bitmap.bmHeight;  
	//为位图内容分配内存  
	/*xxxxxxxx计算位图大小分解一下(解释一下上面的语句)xxxxxxxxxxxxxxxxxxxx 
	//每个扫描行所占的字节数应该为4的整数倍，具体算法为: 
	1int biWidth = (Bitmap.bmWidth*wBitCount) / 32; 
	if((Bitmap.bmWidth*wBitCount) % 32) 
	 biWidth++; //不是整数倍的加1 
	biWidth *= 4;//到这里，计算得到的为每个扫描行的字节数。 
	dwBmBitsSize = biWidth * Bitmap.bmHeight;//得到大小 
	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/  
	  
	hDib = GlobalAlloc(GHND,dwBmBitsSize+dwPaletteSize+sizeof(BITMAPINFOHEADER));  
	lpbi = (LPBITMAPINFOHEADER)GlobalLock(hDib);  
	*lpbi = bi;  
	// 处理调色板  
	hPal = GetStockObject(DEFAULT_PALETTE);  
	if (hPal)  
	{  
	hDC = ::GetDC(NULL);  
	hOldPal=SelectPalette(hDC,(HPALETTE)hPal,FALSE);  
	RealizePalette(hDC);  
	}  
	// 获取该调色板下新的像素值  
	GetDIBits(hDC,hBitmap,0,(UINT)Bitmap.bmHeight,(LPSTR)lpbi+sizeof(BITMAPINFOHEADER)+dwPaletteSize, (BITMAPINFO *)lpbi,DIB_RGB_COLORS);  
	//恢复调色板  
	if (hOldPal)  
	{  
	SelectPalette(hDC, hOldPal, TRUE);  
	RealizePalette(hDC);  
	::ReleaseDC(NULL, hDC);  
	}  
	//创建位图文件  
	fh=CreateFile(lpFileName, GENERIC_WRITE,0, NULL, CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);  
	if (fh==INVALID_HANDLE_VALUE)  
	return ;  
	// 设置位图文件头  
	bmfHdr.bfType = 0x4D42; // "BM"  
	dwDIBSize=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+dwPaletteSize+dwBmBitsSize; 
	bmfHdr.bfSize = dwDIBSize;  
	bmfHdr.bfReserved1 = 0;  
	bmfHdr.bfReserved2 = 0;  
	bmfHdr.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER)+(DWORD)sizeof(BITMAPINFOHEADER)+dwPaletteSize;  
	// 写入位图文件头  
	WriteFile(fh, (LPSTR)&bmfHdr, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);  
	// 写入位图文件其余内容  
	WriteFile(fh, (LPSTR)lpbi, sizeof(BITMAPINFOHEADER)+dwPaletteSize+dwBmBitsSize , &dwWritten, NULL);  
	//清除  
	GlobalUnlock(hDib);  
	GlobalFree(hDib);  
	CloseHandle(fh);  
	return ; 
}