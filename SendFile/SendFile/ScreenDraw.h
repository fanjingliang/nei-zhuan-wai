
#pragma once

#define SCREENWIDTH 1280
#define SCREENHEIGHT 1024

#define WINDOWWIDTH 700
#define WINDOWHEIGHT 600


extern RECT g_picture_rect;

typedef struct
{
	char file[256];
	SYSTEMTIME  tm;		//为了防止同一个文件重复接收，用时间来标记一个文件。
	unsigned int len;
	unsigned int pagecount;
	unsigned int pagecur;
	char data[0];
}fileinfo_t;

typedef struct
{
	unsigned int checksum;
	fileinfo_t info;	
}screenfileinfo_t;

/*
	在文件描述信息框中写入内容
*/
void DrawPos(CDC *pdc);
void DrawPage( CDC *pdc, fileinfo_t * pinfo);
void DrawPageDirect(CDC *pdc, char ** ppagelist, int index);
bool GetPage( char ** out_buf);

char** MakeFilePageList(char *pathfile, char *file, int * out_num);
void FreeFilePageList( char **plist, int num);


void SetPictureRect(LPRECT rc);
void GetPictureRect();

void SaveBitmapToFile(HBITMAP hBitmap, LPSTR lpFileName);