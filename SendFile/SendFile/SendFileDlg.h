// SendFileDlg.h : 头文件
//

#pragma once

#include "ScreenDraw.h"

// CSendFileDlg 对话框
class CSendFileDlg : public CDialog
{
// 构造
public:
	CSendFileDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_SENDFILE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonSend();
	afx_msg void OnBnClickedButtonRecv();

protected:
	CString m_send_file;
	CString m_send_filepath;
	char ** m_pagelist;
	int m_pagelist_count;

	int m_pagelist_index;

	char m_recv_file[256];
	char ** m_recvlist;
	int m_recvlist_count;
	int m_recvlist_cur;
public:
	afx_msg void OnBnClickedButtonSelectfile();
	afx_msg void OnBnClickedOk();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

protected:
	void InitControlPosition();
	void ParseFile(screenfileinfo_t * info);
	void ClearLastFile();
	void UpdateRecvStat(char * file,int cur);
public:
	CString m_static_info;
	afx_msg void OnBnClickedButtonStop();
};
